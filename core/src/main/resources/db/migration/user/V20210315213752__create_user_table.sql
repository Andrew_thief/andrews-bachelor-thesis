CREATE TABLE APP_USER
(
    id         SERIAL PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    last_name  VARCHAR NOT NULL,
    password   VARCHAR NOT NULL
);